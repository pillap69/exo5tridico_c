﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriAB_String_Accent
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] TabPrenom = new string[7] { "éric", "théodore", "éléonore", "theophile", "andre", "annie", "anouk" };
            
            Dictionary<int,char> odicoLettreAccent = new Dictionary<int, char>();
            odicoLettreAccent.Add(201, 'e'); // Code ASCII ISO-8859-1 pour la lettre 'É'
            odicoLettreAccent.Add(232, 'e'); // Code ASCII ISO-8859-1 pour la lettre 'è'
            odicoLettreAccent.Add(233, 'e'); // Code ASCII ISO-8859-1 pour la lettre 'é'
            odicoLettreAccent.Add(235, 'e'); // Code ASCII ISO-8859-1 pour la lettre 'ë'
           
            for (int z = 0; z < TabPrenom.Length - 1; z++)
            {
                for (int i = 0; i < TabPrenom.Length - 1; i++)
                {
                    int cpt = 0; //compteur d'égalité de lettre utilisé dans for des autres lettres
                    string prenom1 = TabPrenom[i];

                    string prenom2 = TabPrenom[i + 1];

                    string lettre1Prenom1 = prenom1.Substring(0, 1);
                    string lettre1Prenom2 = prenom2.Substring(0, 1);

                    char caract1Prenom1 = Convert.ToChar(lettre1Prenom1);
                    char caract1Prenom2 = Convert.ToChar(lettre1Prenom2);

                    char caract1Prenom1SansAccent;
                    Boolean OK1 = odicoLettreAccent.TryGetValue(caract1Prenom1, out caract1Prenom1SansAccent);
                    char caract1Prenom2SansAccent;
                    Boolean OK2 = odicoLettreAccent.TryGetValue(caract1Prenom2, out caract1Prenom2SansAccent);
                    if (OK1) { caract1Prenom1 = caract1Prenom1SansAccent; }
                    if (OK2) { caract1Prenom2 = caract1Prenom2SansAccent; }

                    if (caract1Prenom1 > caract1Prenom2)
                    {
                        //permutation
                        string temp = TabPrenom[i];
                        TabPrenom[i] = TabPrenom[i + 1];
                        TabPrenom[i + 1] = temp;
                    }

                    int positionPrenomCourt;
                    if (caract1Prenom1 == caract1Prenom2)
                    {
                        string prenomPlusCourt;
                        if (TabPrenom[i].Length >= TabPrenom[i + 1].Length)
                        {
                            prenomPlusCourt = TabPrenom[i + 1];
                            positionPrenomCourt = 2;
                        }
                        else
                        {
                            prenomPlusCourt = TabPrenom[i];
                            positionPrenomCourt = 1;
                        }


                        for (int x = 1; x < prenomPlusCourt.Length; x++)
                        {
                            string lettrePrenom1 = TabPrenom[i].Substring(x, 1);
                            string lettrePrenom2 = TabPrenom[i + 1].Substring(x, 1);
                            char caractPrenom1 = Convert.ToChar(lettrePrenom1);
                            char caractPrenom2 = Convert.ToChar(lettrePrenom2);

                            // verification des "e" accentués qui sont remplacés par des "e"
                            char caractPrenom1SansAccent;
                            Boolean OK3 = odicoLettreAccent.TryGetValue(Convert.ToChar(lettrePrenom1), out caractPrenom1SansAccent);
                            char caractPrenom2SansAccent;
                            Boolean OK4 = odicoLettreAccent.TryGetValue(Convert.ToChar(lettrePrenom2), out caractPrenom2SansAccent);
                            if (OK3) { caractPrenom1 = caractPrenom1SansAccent; }
                            if (OK4) { caractPrenom2 = caractPrenom2SansAccent; }

                            /* le else if au lieu de 3 if rend l'égalité implicite avec un else 
                             * autrement il faut préciser le cas de l'égalité dans le if */
                            if (caractPrenom1 > caractPrenom2)
                            {
                                string temp = TabPrenom[i];
                                TabPrenom[i] = TabPrenom[i + 1];
                                TabPrenom[i + 1] = temp;
                                break; // on sort du for la permutation a été effectuée
                            }
                            else if (Convert.ToChar(lettrePrenom1) < Convert.ToChar(lettrePrenom2))
                            {
                                break; // on sort du for c'est déjà dans le bon ordre
                            }
                            else //if (Convert.ToChar(lettrePrenom1) == Convert.ToChar(lettrePrenom2))
                            {
                                cpt++; //compte le nombre de fois ou les lettres sont égales
                            }

                        }

                        /* si le compteur d'égalité est égale à la longeur du plus petit mot des deux moins 1
                          alors toutes les lettres du petit sont communes avec le plus grand */
                        if (cpt == prenomPlusCourt.Length - 1)
                        {
                            /* si la position du plus court est deuxième et donc le plus long est en premier
                             il faut permuter */
                            if (positionPrenomCourt == 2)
                            {
                                string temp = TabPrenom[i];
                                TabPrenom[i] = TabPrenom[i + 1];
                                TabPrenom[i + 1] = temp;
                            }
                        }


                    } // fin if
                }
            }

            // Affichage des valeurs triées
            Console.WriteLine("Les valeurs triées :" + "\n\r");
            int p = 0;
            while (p < TabPrenom.Length)
            {
                Console.WriteLine(p + " " + TabPrenom[p]);
                p++;
            }

            Console.ReadKey();

        } // fin Main
    }
}
