﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriABulleTab_String
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] TabPrenom = new string[7] { "zoe","theodore","andree", "archibalde", "andre", "annie", "anouk" };
            for (int z = 0; z < TabPrenom.Length - 1; z++)
            {
                for (int i = 0; i < TabPrenom.Length - 1; i++)
                {
                    int cpt = 0; //compteur d'égalité de lettre utilisé dans for des autres lettres
                    string prenom1 = TabPrenom[i];

                    string prenom2 = TabPrenom[i + 1];

                    string lettre1Prenom1 = prenom1.Substring(0, 1);
                    string lettre1Prenom2 = prenom2.Substring(0, 1);

                    char caract1Prenom1 = Convert.ToChar(lettre1Prenom1);
                    char caract1Prenom2 = Convert.ToChar(lettre1Prenom2);


                    
                    if (caract1Prenom1 > caract1Prenom2)
                    {
                        //permutation
                        string temp = TabPrenom[i];
                        TabPrenom[i] = TabPrenom[i + 1];
                        TabPrenom[i + 1] = temp;
                    }

                    int positionPrenomCourt;
                    if (caract1Prenom1 == caract1Prenom2)
                    {
                        string prenomPlusCourt;
                        if (TabPrenom[i].Length >= TabPrenom[i + 1].Length)
                        {
                            prenomPlusCourt = TabPrenom[i + 1];
                            positionPrenomCourt = 2;
                        }
                        else
                        {
                            prenomPlusCourt = TabPrenom[i];
                            positionPrenomCourt = 1;
                        }

                        
                        for (int x = 1; x < prenomPlusCourt.Length; x++)
                        {
                            string lettrePrenom1 = TabPrenom[i].Substring(x, 1);
                            string lettrePrenom2 = TabPrenom[i+1].Substring(x, 1);

                            /* le else if au lieu de 3 if rend l'égalité implicite avec un else 
                             * autrement il faut préciser le cas de l'égalité dans le if */
                            if (Convert.ToChar(lettrePrenom1) > Convert.ToChar(lettrePrenom2))
                            {
                                string temp = TabPrenom[i];
                                TabPrenom[i] = TabPrenom[i + 1];
                                TabPrenom[i + 1] = temp;
                                break; // on sort du for la permutation a été effectuée
                            }
                            else if (Convert.ToChar(lettrePrenom1) < Convert.ToChar(lettrePrenom2))
                            {
                                break; // on sort du for c'est déjà dans le bon ordre
                            }
                            else //if (Convert.ToChar(lettrePrenom1) == Convert.ToChar(lettrePrenom2))
                            {
                                cpt++; //compte le nombre de fois ou les lettres sont égales
                            }

                        }

                        /* si le compteur d'égalité est égale à la longeur du plus petit mot des deux moins 1
                          alors toutes les lettres du petit sont communes avec le plus grand */
                        if(cpt == prenomPlusCourt.Length - 1)
                        {
                            /* si la position du plus court est deuxième et donc le plus long est en premier
                             il faut permuter */
                            if (positionPrenomCourt == 2)
                            {
                                string temp = TabPrenom[i];
                                TabPrenom[i] = TabPrenom[i + 1];
                                TabPrenom[i + 1] = temp;
                            }
                        }


                    } // fin if
                }
            }

            // Affichage des valeurs triées
            Console.WriteLine("Les valeurs triées :" + "\n\r");
            int p = 0;
            while(p < TabPrenom.Length)
            {
                Console.WriteLine(p + " " +TabPrenom[p]);
                p++;
            }

            Console.ReadKey();

        } // fin Main
    }
}
