﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using classes_outils;
using System.Data; //Dataset

namespace classes_metier
{

    #region employe : Classe métier
    public static class CemployeAuthentifie
    {
        public static Cemploye oemplAuthentifie;

    }
    public class Cemploye
    {
        private string _id;

        public string id_employe { get; set; }
        public string nom_employe { get; set; }
        public string prenom_employe { get; set; }

        public string login_employe { get; set; }
        public string mdp_employe { get; set; }
        public string adresse_employe { get; set; }
        public int cp_employe { get; set; }
        public string ville_employe { get; set; }
        public DateTime dateEmbauche_employe { get; set; }

        public bool estAuthentifie { get; set; }
        public string Id { get => _id; set => _id = value; }

        public Cemploye(string sid_employe, string snom_employe, string sprenom_employe, string slogin_employe, string smdp_employe, string sadresse_employe, int scp_employe, string sville_employe, DateTime sdateEmbauche_employe)
        {
            id_employe = sid_employe;
            nom_employe = snom_employe;
            prenom_employe = sprenom_employe;
            login_employe = slogin_employe;
            mdp_employe = smdp_employe;
            adresse_employe = sadresse_employe;
            cp_employe = scp_employe;
            ville_employe = sville_employe;
            dateEmbauche_employe = sdateEmbauche_employe;
            estAuthentifie = false;
        }

        

    }

    #endregion

    #region Visiteur : métier et contrôle
    public class Cvisiteur : Cemploye
    {

        public Cvisiteur(string sid_visiteur, string snom_visiteur, string sprenom_visiteur, string slogin_visiteur, string smdp_visiteur, string sadresse_visiteur, int scp_visiteur, string sville_visiteur, DateTime sdateEmbauche_visiteur) 
            : base(sid_visiteur, snom_visiteur, sprenom_visiteur, slogin_visiteur, smdp_visiteur, sadresse_visiteur, scp_visiteur, sville_visiteur, sdateEmbauche_visiteur)
        {
         
        }
        
    }

    public class Cvisiteurs
    {
        Cvisiteur[] tabVisit = new Cvisiteur[27];
        public Dictionary<string, Cvisiteur> ocollDicovisitById;
        public Dictionary<string, Cvisiteur> ocollDicoVisitByLogin;
        public Cvisiteur ovisiteurAuthentifie = null;


        public Cvisiteurs()
        {
            chargerObjetsCvisiteur(); //procédure en dehors du constructeur => peut être rappelée par la suite

        }

        private void chargerObjetsCvisiteur()
        {
            ocollDicovisitById = new Dictionary<string, Cvisiteur>();

            ocollDicoVisitByLogin = new Dictionary<string, Cvisiteur>();

            Cdao odao = new Cdao();
            string query = "SELECT * FROM visiteur";
            MySqlDataReader ord = odao.getReader(query);
            //Cvisiteur ovisiteur  = ocollDicovisit["toto"];
            while (ord.Read())
            {
                Cvisiteur ovisiteur = new Cvisiteur(Convert.ToString(ord["id"]), Convert.ToString(ord["nom"]), 
                    Convert.ToString(ord["prenom"]), Convert.ToString(ord["login"]), 
                    Convert.ToString(ord["mdp"]), Convert.ToString(ord["adresse"]), 
                    Convert.ToInt32(ord["cp"]), Convert.ToString(ord["ville"]), Convert.ToDateTime(ord["dateEmbauche"]));

                ocollDicovisitById.Add(Convert.ToString(ord["id"]), ovisiteur);
                ocollDicoVisitByLogin.Add(Convert.ToString(ord["login"]), ovisiteur);
            }

        }

        public Cvisiteur getVisiteur(string skey) //la clef reçue est soit le "login" soit "id"
        {
            // Cette methode s'adapte au deux dictionnaires
            int valeur;
            Cvisiteur ovisiteur;
            bool trouve = false;

            //dans les id visiteur le deuxième caractère est un entier
            if (int.TryParse(skey.Substring(1, 1), out valeur)) // permet de verifier que le deuxieme caractere de la clef est un int
            {
                trouve = ocollDicovisitById.TryGetValue(skey, out ovisiteur);
            }
            else
            {
                trouve = ocollDicoVisitByLogin.TryGetValue(skey, out ovisiteur);

            }

            if (trouve)
            {
                return ovisiteur;
            }
            else
            {
                return null;
            }

        }

        public void addVisiteur(Cvisiteur sovisiteur)
        {
            Cdao odao = new Cdao();
            string query = "insert into visiteur values('" + sovisiteur.id_employe + "','" + sovisiteur.nom_employe + "','"
                         + sovisiteur.prenom_employe + "','" + sovisiteur.login_employe + "','" + sovisiteur.mdp_employe + "','"
                         + sovisiteur.adresse_employe + "','" + sovisiteur.cp_employe + "','" + sovisiteur.ville_employe + "','"
                         + CtraitementDate.getDateFormatMysql(sovisiteur.dateEmbauche_employe) + "')";
            odao.insertEnreg(query);
            ocollDicovisitById.Add(
               sovisiteur.id_employe, sovisiteur);
            ocollDicoVisitByLogin.Add(
                sovisiteur.login_employe, sovisiteur);
        }

        public void deleteVisiteur(string sid)
        {
            Cdao odao = new Cdao();
            odao.deleteEnreg("delete from visiteur where id='" + sid + "'");
            Cvisiteur ovisiteur = ocollDicovisitById[sid];
            ocollDicoVisitByLogin.Remove(ovisiteur.login_employe);
            ocollDicovisitById.Remove(sid);
        }

        public void updateVisiteur(string squery)
        {
            Cdao odao = new Cdao();
            odao.updateEnreg(squery);
            //met a jour les collection
            chargerObjetsCvisiteur();
        }

        public DataSet getdsVisiteur()
        {
            Cdao odao = new Cdao();
            string query = "select * from visiteur";
            DataSet ods = odao.getDataSet(query);
            return ods;
        }

        /*private static Cvisiteurs Instance = null;
        public static Cvisiteurs getInstance()
        {
            if (Instance == null)
            {
                Instance = new Cvisiteurs();
                return Instance;
            }
            else
            {
                return Instance;
            }


        }*/
    }
    #endregion

    #region Comptable : Classes métier et contrôle

    public class Ccomptable : Cemploye
    {
       
        public Ccomptable(string sid_comptable, string snom_comptable, string sprenom_comptable, string slogin_comptable, string smdp_comptable, string sadresse_comptable, int scp_comptable, string sville_comptable, DateTime sdateEmbauche_comptable)
               : base(sid_comptable, snom_comptable, sprenom_comptable, slogin_comptable, smdp_comptable, sadresse_comptable, scp_comptable, sville_comptable, sdateEmbauche_comptable)
        {

        }

    }

    public class Ccomptables
    {
        public Dictionary<string, Ccomptable> ocollDicoCptable;
        public Dictionary<string, Ccomptable> ocollDicoCptableByLogin;
        public Ccomptable ocomptableAuthentifie = null;

        public Ccomptables()
        {
            ocollDicoCptable = new Dictionary<string, Ccomptable>();
            ocollDicoCptableByLogin = new Dictionary<string, Ccomptable>();
            Cdao odao = new Cdao();
            string query = "SELECT * FROM comptable";
            MySqlDataReader ord = odao.getReader(query);
            
            while (ord.Read())
            {
                Ccomptable ocomptable = new Ccomptable(Convert.ToString(ord["id"]), Convert.ToString(ord["nom"]), Convert.ToString(ord["prenom"]), Convert.ToString(ord["login"]), Convert.ToString(ord["mdp"]), Convert.ToString(ord["adresse"]), Convert.ToInt32(ord["cp"]), Convert.ToString(ord["ville"]), Convert.ToDateTime(ord["dateEmbauche"]));

                ocollDicoCptable.Add(Convert.ToString(ord["id"]), ocomptable);
                ocollDicoCptableByLogin.Add(Convert.ToString(ord["login"]), ocomptable);
            }
        }

        public Ccomptable getComptable(string skey)  //la clef reçue est soit le "login" soit "id"
        {
            // Cette methode s'adapte au deux dictionnaires
            int valeur;
            Ccomptable ocomptable;
            bool trouve = false;

            // Chez les comptables (1 seul comptable pour l'instant !!) le troisième caractère de l'id est un entier
            if (int.TryParse(skey.Substring(2, 1), out valeur)) // permet de verifier que le deuxieme caractere de la clef est un int
            {
                trouve = ocollDicoCptable.TryGetValue(skey, out ocomptable);
            }
            else
            {
                trouve = ocollDicoCptableByLogin.TryGetValue(skey, out ocomptable);

            }

            if (trouve)
            {
                return ocomptable;
            }
            else
            {
                return null;
            }

        }

        public DataSet getdsComptable()
        {
            Cdao odao = new Cdao();
            string query = "select * from comptable";
            DataSet ods = odao.getDataSet(query);
            return ods;
        }

        private static Ccomptables Instance = null;
        public static Ccomptables getInstance()
        {
            if (Instance == null)
            {
                Instance = new Ccomptables();
                return Instance;
            }
            else
            {
                return Instance;
            }


        }
    }

    #endregion

    public class CficheFrais
    {
        public string idVisiteur;
        public string mois;
        //le après le type ? permet de rendre les STRUCT nullable
        public int? nbJustificatifs;
        public Decimal? montantValide;
        public DateTime? date;
        public string idEtat; //reference fraiForfait

        public CficheFrais(string sidVisiteur, string smois, int? snbJustif, Decimal? smtValide, DateTime? sdate, string sidEtat)
        {
            idVisiteur = sidVisiteur;
            mois = smois;
            nbJustificatifs = snbJustif;
            montantValide = smtValide;
            date = sdate;
            idEtat = sidEtat;
        }

    }

    public class CficheFraiss
    {
        private List<CficheFrais> _ocollFicheFrais = new List<CficheFrais>();
        private Dictionary<string, CficheFrais> _ocollFicheFrais_KeyId = new Dictionary<string, CficheFrais>();

        public Dictionary<string, CficheFrais> ocollFicheFrais_KeyId
        {
            get { return _ocollFicheFrais_KeyId; }

        }

        public List<CficheFrais> ocollFicheFrais
        {
            get { return _ocollFicheFrais; }

        }

        private CficheFraiss()
        {


            //utilisation d'une classe outil Cdao
            Cdao odao = new Cdao();
            string query = "select * from fichefrais";
            MySqlDataReader ord = odao.getReader(query);

            //parcours des enregistrements
            while (ord.Read())
            {
                CficheFrais oficheFrais = null;
                if (ord["nbJustificatifs"] is DBNull && ord["montantValide"] is DBNull && ord["dateModif"] is DBNull)
                {
                    oficheFrais = new CficheFrais(Convert.ToString(ord["idVisiteur"]),
                    Convert.ToString(ord["mois"]), 0, 0, null, Convert.ToString(ord["idEtat"]));

                }
                else
                {
                    oficheFrais = new CficheFrais(Convert.ToString(ord["idVisiteur"]),
                       Convert.ToString(ord["mois"]), Convert.ToInt16(ord["nbJustificatifs"]), Convert.ToDecimal(ord["montantValide"]),
                       null, Convert.ToString(ord["idEtat"]));
                }

                _ocollFicheFrais.Add(oficheFrais);
                _ocollFicheFrais_KeyId[oficheFrais.idVisiteur + oficheFrais.mois] = oficheFrais;

            }

        }

        // Autres param en optionnel et si utilisé alors convert dans le vrai type dans la fonction
        public bool ajouterFicheFrais(string sidVisiteur, params string[] stabParamOptionnel)
        {
            try
            {
                string smois = CtraitementDate.getAnneeMoisEnCours();
                string sidEtat = "CR"; // 
                Cdao odao = new Cdao();
                string query = string.Format("insert into fichefrais(idVisiteur,mois,idEtat) values ('{0}','{1}','{2}')", sidVisiteur, smois, sidEtat);
                odao.insertEnreg(query);
                //ajout des ligne de frais forfaits
                string queryFF0 = string.Format("insert into lignefraisforfait values( '{0}','{1}','ETP', '0')", sidVisiteur, smois);
                odao.insertEnreg(queryFF0);

                string queryFF1 = string.Format("insert into lignefraisforfait values( '{0}','{1}','KM', '0')", sidVisiteur, smois);
                odao.insertEnreg(queryFF1);

                string queryFF2 = string.Format("insert into lignefraisforfait values( '{0}','{1}','NUI', '0')", sidVisiteur, smois);
                odao.insertEnreg(queryFF2);

                string queryFF3 = string.Format("insert into lignefraisforfait values( '{0}','{1}','REP', '0')", sidVisiteur, smois);
                odao.insertEnreg(queryFF3);

                //ajout ficheFrais dans les collections
                CficheFrais oficheFrais = new CficheFrais(sidVisiteur, smois, 0, 0, null, sidEtat);
                _ocollFicheFrais.Add(oficheFrais);
                _ocollFicheFrais_KeyId.Add(sidVisiteur + smois, oficheFrais);

                // ajout des lignes de frais forfait dans la collection
                CligneFraisForfaits oLFFs = CligneFraisForfaits.getInstance();

                CligneFraisForfait oLFF0 = new CligneFraisForfait(sidVisiteur, smois, "ETP", 0);
                CligneFraisForfait oLFF1 = new CligneFraisForfait(sidVisiteur, smois, "KM", 0);
                CligneFraisForfait oLFF2 = new CligneFraisForfait(sidVisiteur, smois, "NUI", 0);
                CligneFraisForfait oLFF3 = new CligneFraisForfait(sidVisiteur, smois, "REP", 0);

                oLFFs.AddLigneFraisforfait(oLFF0);
                oLFFs.AddLigneFraisforfait(oLFF1);
                oLFFs.AddLigneFraisforfait(oLFF2);
                oLFFs.AddLigneFraisforfait(oLFF3);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool verifExistFicheFrais(string sidVisiteur, string smois)
        {
            bool reponse = false;

            string query = "select * from fichefrais where idvisiteur='" + sidVisiteur + "' and mois='" + smois + "'";
            Cdao odao = new Cdao();
            MySqlDataReader ord = odao.getReader(query);
            if(ord.HasRows)
            {
                return reponse = true;
            }


            return reponse;
        }

        public string getMontantTotalFrais(string idV, string annee, string mois)
        {

            Cdao odao = new Cdao();
            decimal montantHFF = 0;
            decimal montantFF = 0;
            string query = string.Format("SELECT sum(quantite*montant) FROM lignefraisforfait  INNER JOIN fraisforfait  ON lignefraisforfait.idFraisForfait = fraisforfait.id WHERE idvisiteur='" + idV + "' and mois='" + annee + mois + "';");
            MySqlDataReader TotalFF = odao.getReader(query);
            while (TotalFF.Read())
            {
                if (TotalFF[0] != DBNull.Value)
                {
                    montantFF = Convert.ToDecimal(TotalFF[0]);
                }

            }


            string query2 = string.Format("SELECT sum(montant) from lignefraishorsforfait WHERE idvisiteur='" + idV + "' and mois='" + annee + mois + "';");
            MySqlDataReader TotalHFF = odao.getReader(query2);
            while (TotalHFF.Read())
            {
                if (TotalHFF[0] != DBNull.Value)
                {
                    montantHFF = Convert.ToDecimal(TotalHFF[0]);
                }

            }



            decimal montantTot = montantFF + montantHFF;
            return Convert.ToString(montantTot);

        }

        public void updateFicheFraisVA(string etat, int nbjust, string montantV, string idV, string annee, string mois) //FF VAlidée
        {
            Cdao odao = new Cdao();
            DateTime dateEnFrancais = CtraitementDate.getDateCourante();
            string dateMySql = dateEnFrancais.ToString("yyyy-MM-dd");
            string query = string.Format("update fichefrais set nbjustificatifs ='" + nbjust + "', montantvalide =" + montantV.Replace(',', '.') + ", idetat= '" + etat + "', dateModif='" + dateMySql + "' where idvisiteur='" + idV + "' and mois='" + annee + mois + "';");
            odao.insertEnreg(query);

            if (_ocollFicheFrais_KeyId.ContainsKey(idV + annee + mois))
            {
                CficheFrais oFiche = _ocollFicheFrais_KeyId[idV + annee + mois];
                oFiche.idEtat = etat;
                oFiche.nbJustificatifs = nbjust;
                oFiche.montantValide = Convert.ToDecimal(montantV.Replace('.', ','));

            }



        }

        public void updateFicheFraisRB(string etat, string idV, string annee, string mois) //FF RemBoursée
        {
            Cdao odao = new Cdao();
            DateTime dateEnFrancais = CtraitementDate.getDateCourante();
            string dateMySql = dateEnFrancais.ToString("yyyy-MM-dd");
            string query = string.Format("update fichefrais set idetat= '" + etat + "',dateModif='" + dateMySql + "' where idvisiteur='" + idV + "' and mois='" + annee + mois + "';");
            odao.insertEnreg(query);

            if (_ocollFicheFrais_KeyId.ContainsKey(idV + annee + mois))
            {
                CficheFrais oFiche = _ocollFicheFrais_KeyId[idV + annee + mois];
                oFiche.idEtat = etat;


            }


        }

        public int getAnneeMin()
        {
            DateTime dateEnFrancais = CtraitementDate.getDateCourante();

            int anneeMin = Convert.ToInt32(dateEnFrancais.ToString("yyyy"));
            foreach (CficheFrais oFiche in ocollFicheFrais)
            {
                if (anneeMin > Convert.ToInt32(oFiche.mois.Substring(0, 4)))
                {
                    anneeMin = Convert.ToInt32(oFiche.mois.Substring(0, 4));
                }
            }

            return anneeMin;
        }

        private static CficheFraiss Instance = null;
        public static CficheFraiss getInstance()
        {
            if (Instance == null)
            {
                Instance = new CficheFraiss();
                return Instance;
            }
            else
            {
                return Instance;
            }


        }


    }

    /* -----------------Classe métier et contrôle concerant les frais forfait -------------------------*/
    public class Cfraisforfait
    {
        public string id;
        public string libelle;
        public decimal montant;

        public Cfraisforfait(string sid, string slibelle, decimal smontant)
        {
            id = sid;
            libelle = slibelle;
            montant = smontant;

        }


    }

    public class Cfraisforfaits
    {

        private List<Cfraisforfait> oCollFF = new List<Cfraisforfait>();

        private Cfraisforfaits()
        {
            //utilisation d'une classe outil Cdao
            Cdao odao = new Cdao();
            string query = "select * from fraisforfait";
            MySqlDataReader ord = odao.getReader(query);

            //parcours des enregistrements
            while (ord.Read())
            {
                Cfraisforfait oFFF = new Cfraisforfait((string)ord["id"], (string)ord["libelle"], (decimal)ord["montant"]);

                oCollFF.Add(oFFF);



            }
        }



        public List<Cfraisforfait> getCollFF()
        {
            return oCollFF;
        }

        private static Cfraisforfaits Instance = null;
        public static Cfraisforfaits getInstance()
        {
            if (Instance == null)
            {
                Instance = new Cfraisforfaits();
                return Instance;
            }
            else
            {
                return Instance;
            }
        }
    }


    /* -----------------Classe métier et contrôle concerant les lignes de frais forfait -------------------------*/
    public class CligneFraisForfait
    {

        public string idVisiteur;
        public string mois;
        public string idFraisForfait;
        public int quantite;



        public CligneFraisForfait(string sidVisiteur, string smois, string sidFraisForfait, int squantite)
        {

            idVisiteur = sidVisiteur;
            mois = smois;
            quantite = squantite;
            idFraisForfait = sidFraisForfait;
        }
    }

    public class CligneFraisForfaits
    {
        private List<CligneFraisForfait> oCollLigneFraisForfaits = new List<CligneFraisForfait>();


        private CligneFraisForfaits()
        {

            rechargerCollectionLFF();

        }

        private void rechargerCollectionLFF()
        {
            Cdao odao = new Cdao();
            string query = "select * from lignefraisforfait";

            MySqlDataReader ord = odao.getReader(query);


            //parcours des enregistrements
            while (ord.Read())
            {
                CligneFraisForfait oligne = new CligneFraisForfait((string)ord["idVisiteur"], (string)ord["mois"],
                   (string)ord["idFraisForfait"], (int)ord["quantite"]);

                oCollLigneFraisForfaits.Add(oligne);


            }
        }

        public List<CligneFraisForfait> getCollLigneFraisForfait()
        {
            return oCollLigneFraisForfaits;
        }

        public List<CligneFraisForfait> getCollLigneFraisForfait(string sidVisiteur, string smois)
        {
            List<CligneFraisForfait> olistFF = new List<CligneFraisForfait>();
            foreach (CligneFraisForfait olff in oCollLigneFraisForfaits)
            {
                if (olff.idVisiteur == sidVisiteur.Trim() && olff.mois == smois.Trim())
                {
                    olistFF.Add(olff);
                }
            }
            return olistFF;
        }

        public void AddLigneFraisforfait(CligneFraisForfait oLFF)
        {
            oCollLigneFraisForfaits.Add(oLFF);
        }

        public void UpdateLigneFraisForfait(int squantite, string sidFraisforfait, string sidVisiteur)
        {
            string mois = CtraitementDate.getAnneeMoisEnCours();
            Cdao odao = new Cdao();
            string query = string.Format("update lignefraisforfait set quantite = {0} where idVisiteur='{1}' and mois='{2}' and idFraisForfait = '{3}' ", squantite, sidVisiteur, mois, sidFraisforfait);
            int nbEnregAffecte = odao.updateEnreg(query);
            // modifie les objets dans la collection si le UPDATE a fonctionné
            if (nbEnregAffecte == 1)
            {
                foreach (CligneFraisForfait olff in oCollLigneFraisForfaits)
                {
                    if (olff.idVisiteur == sidVisiteur && olff.idFraisForfait == sidFraisforfait && olff.mois == mois)
                    {
                        olff.quantite = squantite;
                    }
                }
            }
            else
            {
                throw new Exception("Il n'y a pas d'enregistrement correspondant !");

            }
        }

        public DataSet getdsLFF(string sidVisiteur, string sanneeMois)
        {
            Cdao odao = new Cdao();
            string query = "select f.libelle,f.montant,l.quantite from lignefraisforfait l, fraisforfait f where l.idFraisForfait = f.id and l.idVisiteur = '" + sidVisiteur + "' and l.mois = '" + sanneeMois + "'";
            DataSet ods = odao.getDataSet(query);
            return ods;
        }

        private static CligneFraisForfaits Instance = null;
        public static CligneFraisForfaits getInstance()
        {
            if (Instance == null)
            {
                Instance = new CligneFraisForfaits();
                return Instance;
            }
            else
            {
                return Instance;
            }


        }



    }

    #region Ligne de frais hors forfait: classes métier et contrôle
    public class CligneFraisHorsForfait // : ICligneFraisHorsForfait
    {
        public int id;
        public DateTime date;
        public string idVisiteur;
        public string libelle;
        public string mois;
        public decimal montant;


        /*public int Id { get => id; set => id = value; }

        public DateTime Date { get => date; set => date = value; }

        public string IdVisiteur { get => idVisiteur; set => idVisiteur = value; }

        public string Libelle { get => libelle; set => libelle = value; }

        public string Mois { get => mois; set => mois = value; }

        public decimal Montant { get => montant; set => montant = value; } */

        public CligneFraisHorsForfait(int sid, DateTime sdate, string sidVisiteur, string slibelle, string smois, decimal smontant)
        {
            id = sid;
            date = sdate;
            idVisiteur = sidVisiteur;
            libelle = slibelle;
            mois = smois;
            montant = smontant;
        }
    }

    public class CligneFraisHorsForfaits
    {
        private List<CligneFraisHorsForfait> _ocollLigneFraisHorsForfait = new List<CligneFraisHorsForfait>();
        private Dictionary<int, CligneFraisHorsForfait> _ocollLigneFHF_KeyId = new Dictionary<int, CligneFraisHorsForfait>();

        public Dictionary<int, CligneFraisHorsForfait> ocollFicheFHF_KeyId
        {
            get { return _ocollLigneFHF_KeyId; }
            set { _ocollLigneFHF_KeyId = value; }
        }

        public List<CligneFraisHorsForfait> ocollFicheFraisHorsForfait
        {
            get { return _ocollLigneFraisHorsForfait; }
            set { _ocollLigneFraisHorsForfait = value; }
        }

        private CligneFraisHorsForfaits()
        {

            //utilisation d'une classe outil Cdao
            Cdao odao = new Cdao();
            string query = "select * from lignefraishorsforfait";
            MySqlDataReader ord = odao.getReader(query);

            //parcours des enregistrements
            while (ord.Read())
            {
                CligneFraisHorsForfait oligne = new CligneFraisHorsForfait(Convert.ToInt32(ord["id"]),
                   Convert.ToDateTime(ord["date"]), Convert.ToString(ord["idVisiteur"]), Convert.ToString(ord["libelle"]),
                   Convert.ToString(ord["mois"]), Convert.ToDecimal(ord["montant"]));

                _ocollLigneFraisHorsForfait.Add(oligne);
                _ocollLigneFHF_KeyId[oligne.id] = oligne;

            }
            /*ord.Close();
            ocnx.Close(); */
            /* inutible de fermer car l'objet odao est detruit à la fin du conctructeur 
             * de CligneFraisHorsForfait et donc les objets liés par composition aussi  
             c'est à dire ord et ocnx */
        }

        public void effacerLigneHF(int sid)
        {
            Cdao odao = new Cdao();
            string query = "delete from lignefraishorsforfait  where lignefraishorsforfait.id = " + Convert.ToString(sid);
            odao.deleteEnreg(query);

            if (_ocollLigneFHF_KeyId.ContainsKey(sid))
            {
                _ocollLigneFHF_KeyId.Remove(sid);

            }

        }

        public bool ajouterLigneFHF(string sidVisiteur, string slibelle, string smontant)
        {
            // parse en décimale si séparateur est la virgule
            smontant = smontant.Replace('.', ',');
            decimal montant;
            // Verifie que c'est une valeur decimale
            bool conversion = decimal.TryParse(smontant, out montant); //verifie si le montant est un nombre
            if (!conversion)
            {
                return false;

            }
            montant = Math.Round(montant, 2);

            Cdao odao = new Cdao();

            //regle le probleme des datetime avec mysql
            DateTime dateEnFrancais = CtraitementDate.getDateCourante();
            string dateMySql = dateEnFrancais.ToString("yyyy-MM-dd");
            //string dateEnAnglais = DateTime.Parse(dateEnFrancais, anglais).ToShortDateString();
            smontant = smontant.Replace(',', '.');
            string query = "INSERT INTO lignefraishorsforfait(idVisiteur,mois,libelle,date,montant) VALUES ('" +
                sidVisiteur + "'," + "'" + Convert.ToString(CtraitementDate.getAnneeMoisEnCours()) + "'," +
                "'" + slibelle + "'," + "'" + dateMySql + "'," + smontant + ")";

            int nbEnregAffecte = odao.insertEnreg(query);
            //si l'enregistrement dans la base s'est correctement deroule alors j'ajoute la ligne à la collection de ligne HF

            //recup dans la base de l'id de l'insert ci-dessus
            object max = odao.recupMaxChampTable("id", "lignefraishorsforfait");
            CligneFraisHorsForfait oligne = new CligneFraisHorsForfait(Convert.ToInt32(max),
                   Convert.ToDateTime(CtraitementDate.getDateCourante()), sidVisiteur, slibelle,
                   CtraitementDate.getAnneeMoisEnCours().Trim(), montant);

            if (nbEnregAffecte == 1 && max != null)
            {
                _ocollLigneFraisHorsForfait.Add(oligne);
                _ocollLigneFHF_KeyId[oligne.id] = oligne;
                return true;
            }
            return false;
        }

        public Dictionary<int, CligneFraisHorsForfait> getCollLhfByVisiteurEtMois(string sidVisiteur) // Renvoie pour une AnneeMois donnée
        {
            Dictionary<int, CligneFraisHorsForfait> _oCollLhfUnVisiteur_KeyId = new Dictionary<int, CligneFraisHorsForfait>();
            foreach (CligneFraisHorsForfait oligne in _ocollLigneFHF_KeyId.Values)
            {
                if (oligne.idVisiteur == sidVisiteur && oligne.mois.Trim() == CtraitementDate.getAnneeMoisEnCours().Trim())
                {
                    _oCollLhfUnVisiteur_KeyId.Add(oligne.id, oligne);

                }

            }

            if (_oCollLhfUnVisiteur_KeyId.Count != 0)
            {
                return _oCollLhfUnVisiteur_KeyId;
            }

            return null;

        }

        // LA méthode getdsLFHF est surchargée
        public DataSet getdsLFHF(string sidVisiteur)
        {
            Cdao odao = new Cdao();
            string query = "select * from lignefraishorsforfait";
            DataSet ods = odao.getDataSet(query);
            return ods;
        }

        public DataSet getdsLFHF(string sidVisiteur,string sanneeMois)
        {
            Cdao odao = new Cdao();
            string query = "select * from lignefraishorsforfait where idVisiteur = '" + sidVisiteur + "' and mois = '" + sanneeMois + "'";
            DataSet ods = odao.getDataSet(query);
            return ods;
        }

        private static CligneFraisHorsForfaits Instance = null;
        public static CligneFraisHorsForfaits getInstance()
        {
            if (Instance == null)
            {
                Instance = new CligneFraisHorsForfaits();
                return Instance;
            }
            else
            {
                return Instance;
            }


        }

    }

    #endregion


}
