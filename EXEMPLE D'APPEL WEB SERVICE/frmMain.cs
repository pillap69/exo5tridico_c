﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;



using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.Http;

namespace Test_Api_Web_0
{
    public partial class frmMain : Form
    {
        HttpResponseMessage lareponse = null;
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(Form_LoadAsync);
        }

        private void Form_LoadAsync(object sender , EventArgs e)
        {
           

        }

        private void btnAffiche_Click(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //HttpResponseMessage response = client.GetAsync("http://192.168.3.4/api/ligneFHF/getLigneFHF/a17").Result;
            //HttpResponseMessage response = client.GetAsync("http://192.168.3.4").Result;
            //HttpResponseMessage response = client.GetAsync("http://localhost:58949/api/ligneFHF/getLigneFHF/a17").Result;
            HttpResponseMessage response = client.GetAsync("http://192.168.3.4/api/authentification/verification/dandre/oppg5").Result;
            bool ok = response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode) // http://192.168.3.4/api/authentification/verification/dandre/oppg5 
            {
                tbAfficheLigneFHF.Text = response.Content.ReadAsAsync<string>().Result;
                //var olignes = response.Content.ReadAsAsync<IEnumerable<CligneFraisHorsForfait>>().Result;
                //List<CligneFraisHorsForfait> olignes = response.Content.ReadAsAsync<List<CligneFraisHorsForfait>>().Result;
                /*foreach (var l in olignes)
                {
                    tbAfficheLigneFHF.Text = tbAfficheLigneFHF.Text + l.Libelle + " " + l.Montant + "\r\n";

                }*/
                /*string json = response.Content.ReadAsAsync<string>().Result;
                tbAfficheLigneFHF.Text = json;*/
            }

        }

        private void btnCreer_ClickAsync(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();

            CligneFraisHorsForfait oLFHF = new CligneFraisHorsForfait(0, Convert.ToDateTime("15/05/2018"), "a17", "test", "201805", 123);

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //HttpResponseMessage response = client.GetAsync("http://192.168.3.4/api/ligneFHF/getLigneFHF/a17").Result;
            //HttpResponseMessage response = client.GetAsync("http://192.168.3.4").Result;
            string objetJson = JsonConvert.SerializeObject(oLFHF);
            
            var stringContent = new StringContent(objetJson, Encoding.UTF8, "application/json");
            HttpContent ocontent = stringContent;
            //HttpResponseMessage response = client.PostAsync("http://localhost:58949/api/ligneFHF/createLFHF/a17/TP/1000",ocontent).Result;
            var response = client.PostAsync("http://localhost:58949/api/ligneFHF/createLFHF", ocontent).ContinueWith((t) => t.Result.EnsureSuccessStatusCode());
            //HttpResponseMessage response = await client.PostAsJsonAsync("http://localhost:58949/api/ligneFHF/createLFHF", oLFHF);
            //postAsync();

        }

        private async void postAsync()
        { 
            
            HttpClient client = new HttpClient();

            ICligneFraisHorsForfait oLFHF = new CligneFraisHorsForfait(100, Convert.ToDateTime("15/05/218"), "a17", "test", "201805", 123);

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            lareponse = await client.PostAsJsonAsync("http://localhost:58949/api/ligneFHF/createLFHF", oLFHF);
            

        }

        /*
        static async Task<Uri> CreateProductAsync(Product product)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "api/products", product);
            response.EnsureSuccessStatusCode();

            // return URI of the created resource.
            return response.Headers.Location;
        }*/


    }
}
