﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace classes_outils
{
    #region classe_outil accès aux données (Cdao)
    public class Cdao
    {
        private string connectionString = "SERVER=127.0.0.1; DATABASE=gsb; UID=root; PASSWORD=";

        //public object MySqldataAdapter { get; private set; }

        public MySqlDataReader getReader(string squery)
        {

            MySql.Data.MySqlClient.MySqlConnection ocnx = null;
            try { 
                ocnx = new MySqlConnection(connectionString);
                ocnx.Open();
                MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
                MySqlDataReader ord = ocmd.ExecuteReader();
                return ord; 
            }
            finally //permet d'executer un code finalement après une erreur dans le try
            {
                /*ocnx.Close(); Ne doit pas être mis ici car renvoie un MysqlDataReader qui 
                necessite que la connexion reste ouverte */
            }
     

        }

        public int insertEnreg(string squery)
        {
            MySqlConnection ocnx = null;

            try
            {
                ocnx = new MySqlConnection(connectionString);
                ocnx.Open();
                MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
                int nbEnregAffecte = ocmd.ExecuteNonQuery();
                return nbEnregAffecte;
            }
            finally{
                ocnx.Close();
            }
        }

        public int updateEnreg(string squery)
        {
            MySqlConnection ocnx = null;

            try
            {
                ocnx = new MySqlConnection(connectionString);
                ocnx.Open();
                MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
                int nbEnregAffecte = ocmd.ExecuteNonQuery();
                return nbEnregAffecte;
            }
            finally
            {
               ocnx.Close();
            }
        }

        public void deleteEnreg(string squery)
        {
                MySqlConnection ocnx = null;
            try
            {
                ocnx = new MySqlConnection(connectionString);
                ocnx.Open();
                MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
                int nbEnregAffecte = ocmd.ExecuteNonQuery();
            }
            finally
            {
                ocnx.Close();
            }
                
              
        }






    }

    #endregion

    #region Classe_outil traitement des dates (CtraitementDate)
    public static class CtraitementDate
    {
        public static string getMoisEnLettre(int snumMois)
        {
            string[] tabMoisLettre = new string[12];

            tabMoisLettre[0] = "Janvier";
            tabMoisLettre[1] = "Février";
            tabMoisLettre[2] = "Mars";
            tabMoisLettre[3] = "Avril";
            tabMoisLettre[4] = "Mai";
            tabMoisLettre[5] = "Juin";
            tabMoisLettre[6] = "Juillet";
            tabMoisLettre[7] = "Août";
            tabMoisLettre[8] = "Septembre";
            tabMoisLettre[9] = "Octobre";
            tabMoisLettre[10] = "Novembre";
            tabMoisLettre[11] = "Décembre";

            return tabMoisLettre[snumMois - 1];


        }

        public static string getAnneeMoisEnCours()
        {
            //return Convert.ToString(System.DateTime.Now.Year) + Convert.ToString(System.DateTime.Now.Month);
            return DateTime.Now.ToString("yyyyMM");
        }

        public static DateTime getDateCourante()
        {
            return System.DateTime.Now;
        }

        public static string getDateFormatMysql(DateTime sdateFr)
        {

            string dateMySql = sdateFr.ToString("yyyy-MM-dd");
            return dateMySql;

        }
    }
    #endregion
}