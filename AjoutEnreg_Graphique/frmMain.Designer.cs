﻿namespace AjoutEnreg_Graphique
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.lbId = new System.Windows.Forms.Label();
            this.tbId = new System.Windows.Forms.TextBox();
            this.lbNom = new System.Windows.Forms.Label();
            this.lbPrenom = new System.Windows.Forms.Label();
            this.lbAdresse = new System.Windows.Forms.Label();
            this.lbLogin = new System.Windows.Forms.Label();
            this.lbMdp = new System.Windows.Forms.Label();
            this.lbCp = new System.Windows.Forms.Label();
            this.lbDate = new System.Windows.Forms.Label();
            this.tbNom = new System.Windows.Forms.TextBox();
            this.tbPrenom = new System.Windows.Forms.TextBox();
            this.tbAdresse = new System.Windows.Forms.TextBox();
            this.tbMdp = new System.Windows.Forms.TextBox();
            this.tbCp = new System.Windows.Forms.TextBox();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.lbVille = new System.Windows.Forms.Label();
            this.tbVille = new System.Windows.Forms.TextBox();
            this.btnEnreg = new System.Windows.Forms.Button();
            this.mtbDate = new System.Windows.Forms.MaskedTextBox();
            this.lbTitre = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbId
            // 
            this.lbId.AutoSize = true;
            this.lbId.Location = new System.Drawing.Point(59, 51);
            this.lbId.Name = "lbId";
            this.lbId.Size = new System.Drawing.Size(18, 13);
            this.lbId.TabIndex = 0;
            this.lbId.Text = "ID";
            // 
            // tbId
            // 
            this.tbId.Location = new System.Drawing.Point(221, 51);
            this.tbId.Name = "tbId";
            this.tbId.Size = new System.Drawing.Size(192, 20);
            this.tbId.TabIndex = 1;
            // 
            // lbNom
            // 
            this.lbNom.AutoSize = true;
            this.lbNom.Location = new System.Drawing.Point(59, 82);
            this.lbNom.Name = "lbNom";
            this.lbNom.Size = new System.Drawing.Size(32, 13);
            this.lbNom.TabIndex = 2;
            this.lbNom.Text = "NOM";
            // 
            // lbPrenom
            // 
            this.lbPrenom.AutoSize = true;
            this.lbPrenom.Location = new System.Drawing.Point(59, 109);
            this.lbPrenom.Name = "lbPrenom";
            this.lbPrenom.Size = new System.Drawing.Size(54, 13);
            this.lbPrenom.TabIndex = 4;
            this.lbPrenom.Text = "PRENOM";
            // 
            // lbAdresse
            // 
            this.lbAdresse.AutoSize = true;
            this.lbAdresse.Location = new System.Drawing.Point(60, 203);
            this.lbAdresse.Name = "lbAdresse";
            this.lbAdresse.Size = new System.Drawing.Size(58, 13);
            this.lbAdresse.TabIndex = 10;
            this.lbAdresse.Text = "ADRESSE";
            // 
            // lbLogin
            // 
            this.lbLogin.AutoSize = true;
            this.lbLogin.Location = new System.Drawing.Point(59, 147);
            this.lbLogin.Name = "lbLogin";
            this.lbLogin.Size = new System.Drawing.Size(40, 13);
            this.lbLogin.TabIndex = 6;
            this.lbLogin.Text = "LOGIN";
            // 
            // lbMdp
            // 
            this.lbMdp.AutoSize = true;
            this.lbMdp.Location = new System.Drawing.Point(60, 174);
            this.lbMdp.Name = "lbMdp";
            this.lbMdp.Size = new System.Drawing.Size(31, 13);
            this.lbMdp.TabIndex = 8;
            this.lbMdp.Text = "MDP";
            // 
            // lbCp
            // 
            this.lbCp.AutoSize = true;
            this.lbCp.Location = new System.Drawing.Point(60, 234);
            this.lbCp.Name = "lbCp";
            this.lbCp.Size = new System.Drawing.Size(21, 13);
            this.lbCp.TabIndex = 12;
            this.lbCp.Text = "CP";
            // 
            // lbDate
            // 
            this.lbDate.AutoSize = true;
            this.lbDate.Location = new System.Drawing.Point(60, 304);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(109, 13);
            this.lbDate.TabIndex = 16;
            this.lbDate.Text = "DATE D\'EMBAUCHE";
            // 
            // tbNom
            // 
            this.tbNom.Location = new System.Drawing.Point(221, 82);
            this.tbNom.Name = "tbNom";
            this.tbNom.Size = new System.Drawing.Size(190, 20);
            this.tbNom.TabIndex = 3;
            // 
            // tbPrenom
            // 
            this.tbPrenom.Location = new System.Drawing.Point(221, 109);
            this.tbPrenom.Name = "tbPrenom";
            this.tbPrenom.Size = new System.Drawing.Size(191, 20);
            this.tbPrenom.TabIndex = 5;
            // 
            // tbAdresse
            // 
            this.tbAdresse.Location = new System.Drawing.Point(221, 203);
            this.tbAdresse.Name = "tbAdresse";
            this.tbAdresse.Size = new System.Drawing.Size(190, 20);
            this.tbAdresse.TabIndex = 11;
            // 
            // tbMdp
            // 
            this.tbMdp.Location = new System.Drawing.Point(221, 174);
            this.tbMdp.Name = "tbMdp";
            this.tbMdp.PasswordChar = '*';
            this.tbMdp.Size = new System.Drawing.Size(190, 20);
            this.tbMdp.TabIndex = 9;
            // 
            // tbCp
            // 
            this.tbCp.Location = new System.Drawing.Point(221, 234);
            this.tbCp.Name = "tbCp";
            this.tbCp.Size = new System.Drawing.Size(190, 20);
            this.tbCp.TabIndex = 13;
            // 
            // tbLogin
            // 
            this.tbLogin.Location = new System.Drawing.Point(221, 147);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(190, 20);
            this.tbLogin.TabIndex = 7;
            // 
            // lbVille
            // 
            this.lbVille.AutoSize = true;
            this.lbVille.Location = new System.Drawing.Point(64, 272);
            this.lbVille.Name = "lbVille";
            this.lbVille.Size = new System.Drawing.Size(36, 13);
            this.lbVille.TabIndex = 14;
            this.lbVille.Text = "VILLE";
            // 
            // tbVille
            // 
            this.tbVille.Location = new System.Drawing.Point(222, 272);
            this.tbVille.Name = "tbVille";
            this.tbVille.Size = new System.Drawing.Size(188, 20);
            this.tbVille.TabIndex = 15;
            // 
            // btnEnreg
            // 
            this.btnEnreg.Location = new System.Drawing.Point(419, 361);
            this.btnEnreg.Name = "btnEnreg";
            this.btnEnreg.Size = new System.Drawing.Size(173, 44);
            this.btnEnreg.TabIndex = 18;
            this.btnEnreg.Text = "ENREGISTRER";
            this.btnEnreg.UseVisualStyleBackColor = true;
            this.btnEnreg.Click += new System.EventHandler(this.btnEnreg_Click);
            // 
            // mtbDate
            // 
            this.mtbDate.Location = new System.Drawing.Point(222, 304);
            this.mtbDate.Mask = "00/00/0000";
            this.mtbDate.Name = "mtbDate";
            this.mtbDate.Size = new System.Drawing.Size(188, 20);
            this.mtbDate.TabIndex = 17;
            this.mtbDate.ValidatingType = typeof(System.DateTime);
            // 
            // lbTitre
            // 
            this.lbTitre.AutoSize = true;
            this.lbTitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitre.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbTitre.Location = new System.Drawing.Point(180, 9);
            this.lbTitre.Name = "lbTitre";
            this.lbTitre.Size = new System.Drawing.Size(467, 24);
            this.lbTitre.TabIndex = 19;
            this.lbTitre.Text = "SAISIE DES NOUVEAUX VISITEURS MEDICAUX";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(419, 51);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(346, 273);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(149, 460);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(549, 207);
            this.textBox1.TabIndex = 21;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 721);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbTitre);
            this.Controls.Add(this.mtbDate);
            this.Controls.Add(this.btnEnreg);
            this.Controls.Add(this.tbVille);
            this.Controls.Add(this.lbVille);
            this.Controls.Add(this.tbLogin);
            this.Controls.Add(this.tbCp);
            this.Controls.Add(this.tbMdp);
            this.Controls.Add(this.tbAdresse);
            this.Controls.Add(this.tbPrenom);
            this.Controls.Add(this.tbNom);
            this.Controls.Add(this.lbDate);
            this.Controls.Add(this.lbCp);
            this.Controls.Add(this.lbMdp);
            this.Controls.Add(this.lbLogin);
            this.Controls.Add(this.lbAdresse);
            this.Controls.Add(this.lbPrenom);
            this.Controls.Add(this.lbNom);
            this.Controls.Add(this.tbId);
            this.Controls.Add(this.lbId);
            this.ForeColor = System.Drawing.Color.SteelBlue;
            this.Name = "frmMain";
            this.Text = "Enregistrement des visiteur médicaux";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbId;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.Label lbNom;
        private System.Windows.Forms.Label lbPrenom;
        private System.Windows.Forms.Label lbAdresse;
        private System.Windows.Forms.Label lbLogin;
        private System.Windows.Forms.Label lbMdp;
        private System.Windows.Forms.Label lbCp;
        private System.Windows.Forms.Label lbDate;
        private System.Windows.Forms.TextBox tbNom;
        private System.Windows.Forms.TextBox tbPrenom;
        private System.Windows.Forms.TextBox tbAdresse;
        private System.Windows.Forms.TextBox tbMdp;
        private System.Windows.Forms.TextBox tbCp;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.Label lbVille;
        private System.Windows.Forms.TextBox tbVille;
        private System.Windows.Forms.Button btnEnreg;
        private System.Windows.Forms.MaskedTextBox mtbDate;
        private System.Windows.Forms.Label lbTitre;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox1;
    }
}

