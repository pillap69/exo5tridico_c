﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using classes_metier;

namespace AjoutEnreg_Graphique
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent(); // aucun code avant cet appel car cette procédure initialise l'ensemble des contrôles de la feuille
        }

        private void btnEnreg_Click(object sender, EventArgs e)
        {
            // Try dans un try
            /* Le try externe va tester l'instanciation des classes Cvisiteur et Cvisiteurs 
             * il peut y avoir une exception si le SGBDR n'est pas en marche
             le try interne teste l'appel à la méthode addVisiteur de Cvisiteurs qui peut 
             lever une exception pour plusieurs raisons (voir les messages d'exception)*/
           try
           {
                //je mets les déclarations et les instanciation en dehors du try pour la visibilité dans le catch
                Cvisiteurs ovisiteurs;
                Cvisiteur ovisiteur;
                verifControls();
                ovisiteur = new Cvisiteur(tbId.Text, tbNom.Text, tbPrenom.Text,
                   tbLogin.Text, tbMdp.Text, tbAdresse.Text, Convert.ToInt32(tbCp.Text),
                   tbVille.Text, Convert.ToDateTime(mtbDate.Text));
                ovisiteurs = new Cvisiteurs();


                try
                {
                    /* La variable ovisiteurs est vue dans ce contexte car la déclaration est faite dans le try 
                      externe qui est donc commun au try interne*/
                    ovisiteurs.addVisiteur(ovisiteur);
                    effaceContenuContrôle();
                    MessageBox.Show("L'enregistrement a bien été effectué.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    // utilisation d'une structure alternative SWITCH au lieu d'un IF 
                    //le SWITCH à une meilleur lisibilité dans le cas d'un condition sur égalité (==)
                    switch (ex.Message)
                    {
                        // Dans ce cas il n'est pas nécessaire de remove car l'ajout n'a pas eu lieu
                        case "Clef redondante sur dico par ID":
                            MessageBox.Show(ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;

                        case "Clef redondante sur dico par LOGIN":
                            // Si l'erreur est sur Login alors je remove sur Id car celui sur Id a eu lieu
                            ovisiteurs.OcollDicovisitById.Remove(ovisiteur.Id);
                            MessageBox.Show(ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        case "Erreur lors de l'insertion dans la base":
                            // Si l'erreur est sur la base alors je remove dans les deux collections
                            ovisiteurs.OcollDicovisitById.Remove(ovisiteur.Id);
                            ovisiteurs.OcollDicoVisitByLogin.Remove(ovisiteur.Login);
                            MessageBox.Show(ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        default: // Je prévois une exception autre éventuelle
                            MessageBox.Show(ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                }
           }
           catch(Exception exc)
           {
                // Exception sur instanciation des classes Cvisiteur et Cvisiteurs
                // Cvisiteurs est plus en clin à l'exception car il y a un accès base à l'interieur.
                // Par exemple XAMPP (MYSQL) n'est pas démarré
                MessageBox.Show(exc.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
           }
        }

        // procédure private car appelée uniquement dans la classe en cours (this)
        private void verifControls()
        {
            foreach(Control ocontrol in this.Controls)
            {
                // Polymorphisme sur TextBox et MaskTextBox avec TextBoxBase
                // Cette notion sera étudiée plus en détail plus tard dans l'année
                if(ocontrol.Text == string.Empty && ocontrol is TextBoxBase)
                {
                    throw new Exception("Vous n'avez pas saisi tous les champs !");
                }
            }
        }

        private void effaceContenuContrôle()
        {
            foreach (Control ocontrol in this.Controls)
            {
                if (ocontrol is TextBoxBase)
                {
                    ocontrol.Text = string.Empty;
                    //TextBoxBase otb = (TextBoxBase)ocontrol;
                    //otb.Clear();
                }
               
               
            }

        }
    }
}
