﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using classes_metier;

namespace deleteVisiteurByID
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Saisir un id visiteur à supprimer : ");
            string id = Console.ReadLine();
            Cvisiteurs ovisiteurs = new Cvisiteurs();
            ovisiteurs.deleteVisiteur(id);
        }
    }
}
