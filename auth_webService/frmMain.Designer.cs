﻿namespace auth_webService
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnauth = new System.Windows.Forms.Button();
            this.tbAffiche = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnauth
            // 
            this.btnauth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnauth.Location = new System.Drawing.Point(496, 320);
            this.btnauth.Name = "btnauth";
            this.btnauth.Size = new System.Drawing.Size(216, 51);
            this.btnauth.TabIndex = 0;
            this.btnauth.Text = "s\'authentifier";
            this.btnauth.UseVisualStyleBackColor = true;
            this.btnauth.Click += new System.EventHandler(this.Btnauth_Click);
            // 
            // tbAffiche
            // 
            this.tbAffiche.Location = new System.Drawing.Point(91, 50);
            this.tbAffiche.Multiline = true;
            this.tbAffiche.Name = "tbAffiche";
            this.tbAffiche.Size = new System.Drawing.Size(252, 207);
            this.tbAffiche.TabIndex = 1;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbAffiche);
            this.Controls.Add(this.btnauth);
            this.Name = "frmMain";
            this.Text = "Fenêtre de TEST pour test API PHP";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnauth;
        private System.Windows.Forms.TextBox tbAffiche;
    }
}

