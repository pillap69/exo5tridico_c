﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using classes_metier;
// nugget : Newtonsoft.Json
using Newtonsoft.Json;
//nugget : Microsoft.AspNet.WebApi.Client
using System.Net.Http.Formatting; // permet d'obtenir la méthode PostAsJsonAsync


namespace auth_webService
{
    public static class connexionWS
    {
        //static string path = "http://localhost/WS_MVC_PERSO/api.php/visiteur/dandre;
        public static Cvisiteur ovisiteur;
        public static async Task<string> authentificationAsync(string slogin, string smdp)
        {
            System.Net.Http.HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost/");
            ovisiteur = null;
            HttpResponseMessage response = await client.GetAsync("WS_MVC_PERSO/api.php/visiteur/" + slogin + "/" + smdp);
         
            if (response.IsSuccessStatusCode)
            {
                //string jsonVisiteur = await response.Content.ReadAsStringAsync();
                //ovisiteur = Newtonsoft.Json.JsonConvert.DeserializeObject<Cvisiteur>(jsonVisiteur);
                return response.StatusCode.ToString();
            }
            return null;
        }

        //public static async Task<HttpStatusCode> CreateAsync(object sobjet)
        public static async Task<string> CreateAsync(object sobjet)
        {
            string jsonString = JsonConvert.SerializeObject(sobjet);
            System.Net.Http.HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            
            StringContent ocontent = new StringContent(jsonString);
            Type typeObjet = sobjet.GetType();
            string methodeControleur = typeObjet.Name.ToString().Substring(1);
            HttpResponseMessage response2 = await client.PostAsync("WS_MVC_PERSO/api.php/" + methodeControleur, ocontent);
            try
            {
                response2.EnsureSuccessStatusCode();
                return response2.StatusCode.ToString();
            }
            catch
            {
                return response2.StatusCode.ToString();
            }

            // return URI of the created resource.
            //return response.Headers.Location;
            //return response2.StatusCode;
        }

        /*private static async void postAsync()
        {

            HttpClient client = new HttpClient();

            ICligneFraisHorsForfait oLFHF = new CligneFraisHorsForfait(100, Convert.ToDateTime("15/05/218"), "a17", "test", "201805", 123);

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            lareponse = await client.PostAsJsonAsync("http://localhost:58949/api/ligneFHF/createLFHF", oLFHF);


        }*/

    }
}
