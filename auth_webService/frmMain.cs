﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using classes_metier;
using System.Threading;
// nugget : Newtonsoft.Json
using Newtonsoft.Json;

namespace auth_webService
{
    public partial class frmMain : Form
    {
        private Cvisiteur ovisiteur = null;
        public frmMain()
        {
            InitializeComponent();
            
        }

        private void Btnauth_Click(object sender, EventArgs e)
        {
            RunAsync();

        }

        /*public async void RunAsync()
        {
           ovisiteur = await connexionWS.GetVisiteurAsync();
           tbAffiche.Text = ovisiteur.Nom;
        }*/
        public async void RunAsync()
        {
            Cvisiteur ovisiteur = new Cvisiteur("AAA", "test", "test", "test", "test", "test", 34, "test", Convert.ToDateTime("20/12/2001"));
            
            //HttpStatusCode message = await connexionWS.CreateVisiteurAsync(ovisiteur);
            string message = await connexionWS.CreateAsync(ovisiteur);
            tbAffiche.Text = message.ToString();
        }

    }
}
