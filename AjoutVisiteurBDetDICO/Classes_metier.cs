﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using classes_outils;
using System.Data; //Dataset

namespace classes_metier
{



    #region Visiteur : métier et contrôle
    public class Cvisiteur
    {
        // Les attibuts
        private string _id; // je mets un underscore car l'attribut est private
        private string _nom;
        private string _prenom;
        private string _login;
        private string _mdp;
        private string _adresse;
        private int _cp;
        private string _ville;
        private DateTime _dateEmbauche;

        // Les propriétés correspondant aux attributs
        public string Id { get => _id; set => _id = value; }
        public string Nom { get => _nom; set => _nom = value; }
        public string Prenom { get => _prenom; set => _prenom = value; }
        public string Login { get => _login; set => _login = value; }
        public string Mdp { get => _mdp; set => _mdp = value; }
        public string Adresse { get => _adresse; set => _adresse = value; }
        public int Cp { get => _cp; set => _cp = value; }
        public string Ville { get => _ville; set => _ville = value; }
        public DateTime DateEmbauche { get => _dateEmbauche; set => _dateEmbauche = value; }


        public Cvisiteur(string sid, string snom, string sprenom, string slogin, string smdp, string sadresse, int scp, string sville, DateTime sdateEmbauche)
        {
            Id = sid;
            Nom = snom;
            Prenom = sprenom;
            Login = slogin;
            Mdp = smdp;
            Adresse = sadresse;
            Cp = scp;
            Ville = sville;
            DateEmbauche = sdateEmbauche;

        }



    }

    #endregion

    #region Classe métier utilisées comme classe de contrôle (Cvisiteurs)
    public class Cvisiteurs
    {

        private Dictionary<string, Cvisiteur> ocollDicovisitById;
        private Dictionary<string, Cvisiteur> ocollDicoVisitByLogin;



        public Cvisiteurs()
        {
            chargerObjetsCvisiteur(); //procédure en dehors du constructeur => peut être rappelée par la suite

        }

        private void chargerObjetsCvisiteur()
        {
            ocollDicovisitById = new Dictionary<string, Cvisiteur>();

            ocollDicoVisitByLogin = new Dictionary<string, Cvisiteur>();

            Cdao odao = new Cdao();
            string query = "SELECT * FROM visiteur";
            MySqlDataReader ord = odao.getReader(query);
            //Cvisiteur ovisiteur  = ocollDicovisit["toto"];
            while (ord.Read())
            {
                Cvisiteur ovisiteur = new Cvisiteur(Convert.ToString(ord["id"]), Convert.ToString(ord["nom"]),
                    Convert.ToString(ord["prenom"]), Convert.ToString(ord["login"]),
                    Convert.ToString(ord["mdp"]), Convert.ToString(ord["adresse"]),
                    Convert.ToInt32(ord["cp"]), Convert.ToString(ord["ville"]), Convert.ToDateTime(ord["dateEmbauche"]));

                ocollDicovisitById.Add(Convert.ToString(ord["id"]), ovisiteur);
                ocollDicoVisitByLogin.Add(Convert.ToString(ord["login"]), ovisiteur);
            }

        }

        public void addVisiteur(Cvisiteur sovisiteur)
        {
            Cdao odao = new Cdao();
            string query = "insert into visiteur values('" + sovisiteur.Id + "','" + sovisiteur.Nom + "','"
                         + sovisiteur.Prenom + "','" + sovisiteur.Login + "','" + sovisiteur.Mdp + "','"
                         + sovisiteur.Adresse + "','" + sovisiteur.Cp + "','" + sovisiteur.Ville + "','"
                         + CtraitementDate.getDateFormatMysql(sovisiteur.DateEmbauche) + "')";
            odao.insertEnreg(query);
            ocollDicovisitById.Add(
               sovisiteur.Id, sovisiteur);
            ocollDicoVisitByLogin.Add(
                sovisiteur.Login, sovisiteur);
        }

        public void deleteVisiteur(string sid)
        {
            Cdao odao = new Cdao();
            odao.deleteEnreg("delete from visiteur where id='" + sid + "'");
            Cvisiteur ovisiteur = ocollDicovisitById[sid];
            ocollDicoVisitByLogin.Remove(ovisiteur.Login);
            ocollDicovisitById.Remove(sid);
        }




    }

        #endregion







}
